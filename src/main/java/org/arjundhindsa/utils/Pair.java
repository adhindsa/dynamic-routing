package org.arjundhindsa.utils;


public class Pair<K,V> {

    private final K x;
    private final V y;

    public Pair(K x, V y) {
        this.x = x;
        this.y = y;
    }

    public K getX() {
        return x;
    }

    public V getY() {
        return y;
    }

    public String toString() {
        return x.toString() + "," + y.toString();
    }

    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        if(!(o instanceof Pair)) {
            return false;
        }
        Pair p = (Pair)o;
        if (!x.getClass().equals(p.getX().getClass())) {
            return false;
        }
        if (!y.getClass().equals(p.getY().getClass())) {
            return false;
        }
        if(this.getX().equals(p.getX()) && this.getY().equals(p.getY())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (x ==null? 0 : x.hashCode());
        result = 31 * result + (y ==null? 0 : y.hashCode());
        return result;
    }
}
