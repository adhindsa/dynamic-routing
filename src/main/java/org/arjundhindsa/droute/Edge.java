package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public class Edge implements EdgeInterface {

    private static final Logger LOG = Logger.getLogger("Edge");
    private final NodeInterface fromNode;
    private final NodeInterface toNode;
    private final java.util.List<CoordinatesInterface> coordinatesOnEdge;
    private final Set<CoordinatesInterface> slowCoordinates;
    private final ConcurrentLinkedQueue<VehicleInterface> vehicles;
    private final int id;
    private final ConcurrentHashMap<CoordinatesInterface,CoordinatesInterface> stoppedCoordianates;

    public Edge(int id, NodeInterface fromNode, NodeInterface toNode) {
        vehicles = new ConcurrentLinkedQueue<VehicleInterface>();
        slowCoordinates = new HashSet<CoordinatesInterface>();
        stoppedCoordianates = new ConcurrentHashMap<CoordinatesInterface,CoordinatesInterface>();
        coordinatesOnEdge = new ArrayList<CoordinatesInterface>();
        this.id = id;
        this.fromNode = fromNode;
        this.toNode = toNode;
        populateCoordinatesOnEdge();
    }

    private void populateCoordinatesOnEdge() {
        CoordinatesInterface coordinates = getStartNode().getCoordinates();
        while(coordinates != null) {
            coordinatesOnEdge.add(coordinates);
            if(coordinates.equals(getEndNode().getCoordinates())) {
                break;
            }
            coordinates = getNextCoordinates(coordinates);
        }
    }

    public String toString() {
        return "["+ fromNode.toString()+"," + toNode.toString()+"]";
    }

    @Override
    public Pair<NodeInterface, NodeInterface> getEdgeNodes() {
        return new Pair(fromNode, toNode);
    }

    @Override
    public NodeInterface getStartNode() {
        return fromNode;
    }

    @Override
    public NodeInterface getEndNode() {
        return toNode;
    }

    @Override
    public int getEdgeId() {
        return id;
    }

    public CoordinatesInterface getNextCoordinates(CoordinatesInterface currentCoordinates) {
        if(isHorizontalEdge()) {
            int direction = (toNode.getCoordinates().getX() - fromNode.getCoordinates().getX());
            int addValue = direction > 0 ? 1 : -1;
            int newX = currentCoordinates.getX()+addValue;
            Pair<Integer, Integer> nextCoordinatesPair = new Pair(newX, currentCoordinates.getY());
            return new Coordinates(nextCoordinatesPair);
        }
        if(isVerticleEdge()) {
            int direction = (toNode.getCoordinates().getY() - fromNode.getCoordinates().getY());
            int addValue = direction > 0 ? 1 : -1;
            int newY = currentCoordinates.getY()+addValue;
            Pair<Integer, Integer> nextCoordinatesPair = new Pair(currentCoordinates.getX(), newY);
            return new Coordinates(nextCoordinatesPair);
        }
        return null;
    }

    @Override
    public void add(VehicleInterface vehicle) {
        vehicles.add(vehicle);
    }

    public boolean isOccupied(CoordinatesInterface coordinates) {
        for(VehicleInterface vehicle : vehicles) {
            if(vehicle.getCurrentLocation().equals(coordinates)) {
                return true;
            }
        }
        return false;
    }

    public boolean stopped() {
        if(stoppedCoordianates.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void remove(VehicleInterface vehicle) {
        boolean removed = vehicles.remove(vehicle);
        if( ! removed ) {
            LOG.severe("*** Failed to remove vehicle " + vehicle.getVehicleID() + " from edge " + this.toString());
        }
    }

    @Override
    public boolean contains(CoordinatesInterface coordinates) {
        for(CoordinatesInterface c : coordinatesOnEdge) {
            if(c.equals(coordinates)){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getEdgeWeight() {
        if(stoppedCoordianates.size() > 0) {
            return Integer.MAX_VALUE/2;
        }
        if(slowCoordinates.size() > 0 ) {
            return coordinatesOnEdge.size() + slowCoordinates.size()*Config.SLOW_SPEED_WEIGHT;
        }
        return coordinatesOnEdge.size();
    }

    @Override
    public void setSlowCoordinates(CoordinatesInterface coordinates) {
        slowCoordinates.add(coordinates);
    }

    @Override
    public void setStoppedCoordinates(CoordinatesInterface coordinates) {
        stoppedCoordianates.put(coordinates, coordinates);
    }

    @Override
    public void clearEdge() {
        slowCoordinates.clear();
        stoppedCoordianates.clear();
    }

    @Override
    public void clearEdge(CoordinatesInterface coordinates) {
        stoppedCoordianates.remove(coordinates);
    }

    private boolean isHorizontalEdge() {
        CoordinatesInterface p1 = fromNode.getCoordinates();
        CoordinatesInterface p2 = toNode.getCoordinates();
        if(p1.getY() == p2.getY()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isVerticleEdge() {
        CoordinatesInterface p1 = fromNode.getCoordinates();
        CoordinatesInterface p2 = toNode.getCoordinates();
        if(p1.getX() == p2.getX()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void draw(Graphics2D g2d) {
        g2d.setColor(Color.BLACK);
        if(isHorizontalEdge()) {
            drawHorizontalEdge(g2d);
        } else {
            drawVerticleEdge(g2d);
        }
    }

    private void drawVerticleEdge(Graphics2D g2d) {
        CoordinatesInterface p1 = fromNode.getCoordinates();
        CoordinatesInterface p2 = toNode.getCoordinates();
        int start = Math.min(p1.getY(), p2.getY());
        int end = Math.max(p1.getY(), p2.getY());
        int x = p1.getX();
        for( int i=start+1; i < end; i++ ) {
            g2d.fillRect(x*Config.NODE_WIDTH, i*Config.NODE_HEIGHT, Config.NODE_WIDTH, Config.NODE_HEIGHT);
        }
    }

    private void drawHorizontalEdge(Graphics2D g2d) {
        CoordinatesInterface p1 = fromNode.getCoordinates();
        CoordinatesInterface p2 = toNode.getCoordinates();
        int start = Math.min(p1.getX(), p2.getX());
        int end = Math.max(p1.getX(), p2.getX());
        int y = p1.getY();
        for( int i=start+1; i < end; i++ ) {
            g2d.fillRect(i*Config.NODE_WIDTH, y*Config.NODE_HEIGHT, Config.NODE_WIDTH, Config.NODE_HEIGHT);
        }
    }

    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        if(this == o) {
            return true;
        }
        if(!(o instanceof Edge)) {
            return false;
        }
        Edge e = (Edge)o;
        if(fromNode.equals(e.fromNode) && toNode.equals(e.toNode) && id == e.id) {
            return true;
        }
        return false;
    }
}
