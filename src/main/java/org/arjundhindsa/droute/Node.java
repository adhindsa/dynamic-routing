package org.arjundhindsa.droute;

import java.awt.*;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public class Node implements NodeInterface {

    private static final Logger LOG = Logger.getLogger("Node");
    private final int nodeId;
    private final CoordinatesInterface coordinates;

    public Node(int nodeId, CoordinatesInterface coordinates ) {
        if(coordinates == null) {
            throw new IllegalArgumentException("coordinates of a node cannot be null: Id " + nodeId);
        }
        this.nodeId = nodeId;
        this.coordinates = coordinates;
        LOG.info("Created Node: " + coordinates.getX() + "," + coordinates.getY());
    }

    @Override
    public int getId() {
        return nodeId;
    }

    @Override
    public CoordinatesInterface getCoordinates() {
        return coordinates;
    }

    @Override
    public void draw(Graphics2D g2d) {
        g2d.setColor(Color.RED);
        g2d.drawRect(coordinates.getX()* Config.NODE_WIDTH,
                coordinates.getY()*Config.NODE_HEIGHT,
                Config.NODE_WIDTH,
                Config.NODE_HEIGHT);
    }

    public String toString() {
        return "(" + coordinates.toString() +")";
    }

    public boolean equals(Object o) {
        if( o == null ) {
            return false;
        }
        if(!(o instanceof Node)) {
            return false;
        }
        Node n = (Node)o;
        if( n == this ) {
            return true;
        }
        if(nodeId == n.nodeId && coordinates.equals(n.coordinates)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + nodeId;
        result = 31 * result + (coordinates == null ? 0 : coordinates.hashCode());
        return result;
    }
}
