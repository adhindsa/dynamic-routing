package org.arjundhindsa.droute;

import java.util.ArrayList;
import java.util.List;

/**
 * @author adhindsa
 */
public class Route implements RouteInterface {

    private List<EdgeInterface> routeEdges;

    public Route(List<EdgeInterface> nodesOnPath) {
//        if(nodesOnPath == null || nodesOnPath.size() == 0) {
//            throw new RuntimeException("Could not create route");
//        }
        routeEdges = new ArrayList<EdgeInterface>(nodesOnPath);
    }

    @Override
    public List<EdgeInterface> getEdges() {
        return routeEdges;
    }

    @Override
    public List<EdgeInterface> getRemainingEdges(EdgeInterface currentEdge) {
        List<EdgeInterface> remainingEdges = new ArrayList<EdgeInterface>();
        boolean found = false;
        for(int i=0; i < routeEdges.size(); i++) {
            if(!found) {
                if (routeEdges.get(i).equals(currentEdge)) {
                    found = true;
                }
                continue;
            }
            remainingEdges.add(routeEdges.get(i));
        }
        return remainingEdges;
    }

    public EdgeInterface getNextEdge(EdgeInterface currentEdge) {
        boolean currentEdgeFound = false;
        for(int i=0; i < routeEdges.size(); i++) {
            if(!currentEdgeFound) {
                if (routeEdges.get(i).equals(currentEdge)) {
                    currentEdgeFound = true;
                }
                continue;
            }
            return routeEdges.get(i);
        }
        return null;
    }

    @Override
    public int getRouteLength() {
        return routeEdges.size();
    }

    String route;
    public String toString() {
        if(route != null) {
            return route;
        }
        route = "{";
        for(EdgeInterface edge: routeEdges) {
            route = route + edge.toString();
        }
        route = route + "}";
        return route;
    }
}
