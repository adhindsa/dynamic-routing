package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */

import java.awt.*;

/**
 * Any shape on the map that can be drawn should implement this
 * interface.
 */
public interface DrawableInterface {
    void draw(Graphics2D g2d);
}
