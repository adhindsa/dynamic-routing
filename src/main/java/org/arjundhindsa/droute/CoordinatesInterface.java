package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */
public interface CoordinatesInterface {
    int getX();
    int getY();
}
