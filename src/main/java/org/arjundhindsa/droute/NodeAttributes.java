package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;

/**
 * @adhindsa
 */
public class NodeAttributes {
    private final String name;
    private final Pair<Integer, Integer> coordinates;

    public NodeAttributes(String name, Pair<Integer, Integer> coordinates) {
        this.name = name;
        this.coordinates = coordinates;
    }

    public String getName() {
        return name;
    }

    public Pair<Integer, Integer> getCoordinates() {
        return coordinates;
    }
}
