package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */

/**
 * Directed Acyclic Graph Shortest path calculator
 */
public class DAGShortestPathCalculator {
    private static final Logger LOG = Logger.getLogger("DAGShortestPathCalculator");
    private static final int DONE = -1;
    private final List<NodeInterface> nodes;
    private final List<EdgeInterface> edges;
    private final Map<Integer, NodeInterface> nodeMap;
    private final Map<NodeInterface, List<EdgeInterface>> edgesStartingFromNode;
    private final Map<Pair<NodeInterface, NodeInterface>, EdgeInterface> pairsToEdgeMap;

    public DAGShortestPathCalculator(List<NodeInterface> nodes, List<EdgeInterface> edges) {
        this.nodes = nodes;
        this.edges = edges;
        nodeMap = new HashMap<Integer, NodeInterface>();
        edgesStartingFromNode = new HashMap<NodeInterface, List<EdgeInterface>>();
        pairsToEdgeMap = new HashMap<Pair<NodeInterface, NodeInterface>, EdgeInterface>();
        buildNodesMap();
        buildEdgesStartingFromNode();
    }

    private void buildEdgesStartingFromNode() {
        for(EdgeInterface edge: edges) {
            Pair<NodeInterface, NodeInterface> nodePair = new Pair(edge.getStartNode(), edge.getEndNode());
            pairsToEdgeMap.put(nodePair, edge);
            NodeInterface start = edge.getStartNode();
            List<EdgeInterface> edges = edgesStartingFromNode.get(start);
            if(edges == null) {
                edges = new ArrayList<EdgeInterface>();
                edgesStartingFromNode.put(start, edges);
            }
            edges.add(edge);
        }
        printEdgesStartingFromNodes();
    }

    private void buildNodesMap() {
        for(NodeInterface node: nodes) {
            nodeMap.put(node.getId(), node);
        }
    }

    private void printEdgesStartingFromNodes() {
        for(NodeInterface node: edgesStartingFromNode.keySet()) {
            printEdgesStartingFromNode(node);
        }
    }

    private void printEdgesStartingFromNode(NodeInterface node) {
        StringBuilder sb = new StringBuilder();
        List<EdgeInterface> edges = edgesStartingFromNode.get(node);
        for(EdgeInterface edge: edges) {
            sb.append(edge.toString());
        }
        LOG.info("Edges starting from " + node.toString() + ": " + sb.toString());
    }

    public RouteInterface getShortestRoute(NodeInterface start, NodeInterface end) {
        RouteInterface shortestRoute = null;
        int distance[] = new int[nodes.size()];
        int prevNodes[] = new int[nodes.size()];
        distance[start.getId()] = 0;
        initializeDistanceAndParentNodes(distance, prevNodes);
        PriorityQueue<NodeWithDistance> unsettledNodes = new PriorityQueue<NodeWithDistance>();
        Set<NodeInterface> settledNodes = new HashSet<NodeInterface>();
        unsettledNodes.add(new NodeWithDistance(start, 0));
        distance[start.getId()] = 0;
        prevNodes[start.getId()] = -1;
        boolean done = false;
        while(!done) {
            NodeWithDistance node = unsettledNodes.poll();
            if(node == null) {
                done = true;
                continue;
            }
            List<EdgeInterface> edges = edgesStartingFromNode.get(node.node);
            if(edges == null) {
                continue;
            }
            addToUnsettledNodes(edges, settledNodes, unsettledNodes,
                    node.distanceFromStarting, distance, prevNodes);
            settledNodes.add(node.node);
        }
        shortestRoute = buildShortesRoute(end, prevNodes);
        LOG.fine("Shortest Route: " + shortestRoute.toString());
        return shortestRoute;
    }

    private RouteInterface buildShortesRoute(NodeInterface end, int[] prevNodes) {
        Stack<NodeInterface> stack = new Stack<NodeInterface>();
        boolean done = false;
        NodeInterface node = end;
        while(!done) {
            stack.push(node);
            int nodeId = node.getId();
            int prevNodeId = prevNodes[nodeId];
            if(prevNodeId == DONE) {
                done = true;
                continue;
            }
            node = nodeMap.get(prevNodeId);
        }
        List<EdgeInterface> routeNodes = new ArrayList<EdgeInterface>();
        NodeInterface fromNode = stack.pop();
        while(!stack.empty()) {
            NodeInterface toNode = stack.pop();
            Pair<NodeInterface, NodeInterface> nodePair = new Pair(fromNode, toNode);
            EdgeInterface edge = pairsToEdgeMap.get(nodePair);
            routeNodes.add(edge);
            fromNode = toNode;
        }
        Route shortestRoute = new Route(routeNodes);
//        LOG.info("route: " + shortestRoute.toString());
        return shortestRoute;
    }

    private void addToUnsettledNodes(List<EdgeInterface> edges,
                                     Set<NodeInterface> settledNodes, PriorityQueue<NodeWithDistance> unsettledNodes,
                                     int startingDistance,
                                     int[] distances,
                                     int[] prevNodes) {
        for(EdgeInterface edge : edges) {
            NodeInterface node = edge.getEndNode();
            if(settledNodes.contains(node)) {
                continue;
            }
            int edgeWeight = edge.getEdgeWeight();
            int totalDistance = startingDistance + edgeWeight;
            if (totalDistance < distances[node.getId()]) {
                distances[node.getId()] = totalDistance;
                prevNodes[node.getId()] = edge.getStartNode().getId();
            }

//            if(!settledNodes.contains(node)) {
                NodeWithDistance n = new NodeWithDistance(node, distances[node.getId()]);
                unsettledNodes.offer(n);
//            }
        }
    }

    private void initializeDistanceAndParentNodes(int[] distance, int prevNodes[]) {
        for(int i=0; i < distance.length; i++ ) {
            distance[i] = Integer.MAX_VALUE/2;
            prevNodes[i] = -1;
        }
    }

    private class NodeWithDistance implements Comparable<NodeWithDistance>{
        private final NodeInterface node;
        private final int distanceFromStarting;

        public NodeWithDistance(NodeInterface node, int distanceFromStarting) {
            this.node = node;
            this.distanceFromStarting = distanceFromStarting;
        }

        @Override
        public boolean equals(Object o) {
            if(o == null) {
                return false;
            }
            if( this == o) {
                return true;
            }
            NodeWithDistance nwd = (NodeWithDistance)o;
            if(node.equals(nwd.node) && distanceFromStarting == nwd.distanceFromStarting) {
                return true;
            }
            return false;
        }

        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + distanceFromStarting;
            result = 31 * result + (node == null ? 0 : node.hashCode());
            return result;
        }

        @Override
        public int compareTo(NodeWithDistance o) {
            if(distanceFromStarting < o.distanceFromStarting) {
                return -1;
            }
            if(distanceFromStarting > o.distanceFromStarting) {
                return 1;
            }
            return 0;
        }
    }
}
