package org.arjundhindsa.droute;

import java.util.List;

/**
 * @author adhindsa
 */

/**
 * IRoute represents a list of {@link NodeInterface}.
 */
public interface RouteInterface {
    List<EdgeInterface> getEdges();
    List<EdgeInterface> getRemainingEdges(EdgeInterface currentEdge);
    EdgeInterface getNextEdge(EdgeInterface currentEdge);
    int getRouteLength();
}
