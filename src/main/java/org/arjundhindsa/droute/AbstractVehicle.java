package org.arjundhindsa.droute;

import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public abstract class AbstractVehicle implements VehicleInterface {

    private static final Logger LOG = Logger.getLogger("AbstractVehicle");
    protected final int uniqueVehicleId;
    protected EdgeInterface currentEdge;
    protected CoordinatesInterface currentLocation;
    protected RouteInterface route;
    protected State state;
    protected NodeInterface destination;
    protected RouteType routeType;
    protected volatile boolean movable;

    public AbstractVehicle(int uniqueId) {
        this.uniqueVehicleId = uniqueId;
        movable = false;
    }

    @Override
    public void removeFromMap() {
        currentEdge.remove(this);
        currentLocation = null;
    }

    @Override
    public CoordinatesInterface getCurrentLocation() {
        return currentLocation;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public int getVehicleID() {
        return uniqueVehicleId;
    }


    @Override
    public void setLocation(CoordinatesInterface coordinates) {
        this.currentLocation = coordinates;
    }

    @Override
    public void setDestination(NodeInterface destination) {
        this.destination = destination;
    }

    @Override
    public RouteType getRouteType() {
        return routeType;
    }

    @Override
    public Config.MoveStatus move(java.util.List<CoordinatesInterface> stoppedAreas, java.util.List<CoordinatesInterface> slowAreas) {
        if(! movable) {
            return Config.MoveStatus.STUCK;
        }
        if(currentLocation == null) {
            return Config.MoveStatus.STUCK;
        }
        CoordinatesInterface nextLocation = currentEdge.getNextCoordinates(currentLocation);
        if( stoppedAreas.contains(nextLocation)) {
            return Config.MoveStatus.STUCK;
        }

        if(reachedEndOfEdge(nextLocation)) {
            EdgeInterface nextEdge = route.getNextEdge(currentEdge);
            if(nextEdge != null) {
                LOG.fine("next edge: " + nextEdge.toString());
                if(nextEdge.stopped()) {
                    RouteInterface newRoute = getNewRoute(nextEdge.getStartNode());
                    if(newRoute != null && newRoute.getEdges().size() > 0) {
                        this.route = newRoute;
                        nextEdge = route.getEdges().get(0);
                    } else {
                        LOG.info("New route not available...continue on existing route");
                    }
                }
                if(nextEdge.isOccupied(nextEdge.getStartNode().getCoordinates())){
                    LOG.fine("**** STUCK ****");
                    currentEdge.setStoppedCoordinates(currentLocation);
                    return Config.MoveStatus.STUCK;
                }
                currentEdge.remove(this);
                currentEdge.clearEdge(currentLocation);
                currentEdge = nextEdge;
                LOG.fine("Vehicle is on next Edge now: " + currentEdge.toString());
                nextLocation = currentEdge.getStartNode().getCoordinates();
                currentEdge.add(this);
            } else {
                LOG.fine("Vehicle is off the Map: id " + getVehicleID());
                currentEdge.remove(this);
                return Config.MoveStatus.REACHED_DESTINATION;
            }
        } else {
            if(currentEdge.isOccupied(nextLocation)) {
                LOG.fine("**** STUCK ****");
                return Config.MoveStatus.STUCK;
            }
        }
        LOG.fine("Moving vehicle from " + currentLocation + " to " + nextLocation );
        currentLocation = nextLocation;
        return Config.MoveStatus.MOVED;
    }

    protected abstract RouteInterface getNewRoute(NodeInterface startNode);

    private boolean reachedEndOfEdge(CoordinatesInterface nextLocation) {
        return nextLocation.equals(currentEdge.getEndNode().getCoordinates());
    }
}
