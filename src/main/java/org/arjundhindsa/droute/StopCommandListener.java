package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */
public interface StopCommandListener {
    void onStopPressed();
}
