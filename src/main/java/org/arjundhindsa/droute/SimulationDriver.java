package org.arjundhindsa.droute;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public class SimulationDriver {

    private static final Logger LOG = Logger.getLogger("SimulationDriver");

    private static JLabel mapLabel;
    private static JLabel controlLabel;

    public static void main(String[] args) {

        if(args.length < 2) {
            LOG.log(Level.SEVERE, "Missing paramaters");
            LOG.log(Level.SEVERE, "Usage: java SimulationDriver --config <configFilePath>");
            System.exit(1);
        }
        LOG.info("Starting SimulationDriver");

        final String configFile = getConfigFile(args);
        if(configFile == null) {
            throw new IllegalArgumentException("ConfigFile name not found");
        }

        final PropertyReader propertyReader = new PropertyReader(configFile);
        try {
            propertyReader.load();

            final java.util.List<NodeInterface> nodes = propertyReader.getNodes();
            final java.util.List<EdgeInterface> edges = propertyReader.getEdges();
            final java.util.List<RouteInterface> dumbRoutes = propertyReader.getDumbRoutes();
            final java.util.List<NodeAttributes> nodeAttributeses = propertyReader.getAllNodesAttributes();

            EventQueue.invokeLater(new Runnable() {

                @Override
                public void run() {
                    JFrame frame = new JFrame("Dynamic Routing Example");
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.setSize(Config.GRID_WIDTH, Config.GRID_HEIGHT);

                    controlLabel = new JLabel("Controls", JLabel.CENTER);
                    controlLabel.setForeground(Color.WHITE);
                    mapLabel = new JLabel("           Map", JLabel.CENTER);
                    mapLabel.setForeground(Color.WHITE);

                    Map2D map = new Map2D(nodes, edges, dumbRoutes, nodeAttributeses);
                    map.add(mapLabel);
                    map.setDefaultStartNode(propertyReader.getDefaultStartNode());
                    map.setDefaultEndNode(propertyReader.getDefaultEndNode());

                    ControlPanel controls = new ControlPanel();
                    controls.setActionListener(map);
                    controls.setSlowCommandListener(map);
                    controls.setStopCommandListener(map);
                    controls.add(controlLabel);

                    frame.add(controls, BorderLayout.EAST);
                    frame.add(map, BorderLayout.CENTER);
                    frame.setVisible(true);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String getConfigFile(String[] args) {
        for(int i=0; i < args.length; i++) {
            if(args[i].equals("--config")) {
                return args[++i];
            }
        }
        return null;
    }
}
