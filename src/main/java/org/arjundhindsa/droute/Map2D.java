package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public class Map2D extends JPanel implements ActionListener,
        SlowCommandListener, StopCommandListener, ClearSlowOrStopCommandListener {

    private static final Logger LOG = Logger.getLogger("Map2D");
    /*
     * This timer gives a sense of time on the graph when vehicles
     * move from a node to another node. We are running this timer
     * that gets called every unit tick of time. All the components
     * such as vehicles, or paths on the graph gets updated when
     * a tick is called by the timer.
     */
    private ScheduledThreadPoolExecutor timer;

    private Random random = new Random();
    private AtomicInteger x1 = new AtomicInteger();
    private AtomicInteger y1 = new AtomicInteger();
    private AtomicInteger x2 = new AtomicInteger();
    private AtomicInteger y2 = new AtomicInteger();

    // All the nodes in the map. Here the node has the same meaning
    // as the node on a graph
    private java.util.List<NodeInterface> nodes;

    private final java.util.List<NodeAttributes> nodeAttributes;

    // All the edges in the map. Edges join two nodes.
    private java.util.List<EdgeInterface> edges;

    // All the vehicles currently on the Map
    private java.util.List<VehicleInterface> vehicles;

    private java.util.List<VehicleInterface> intelligentVehicles;

    private final java.util.List<RouteInterface> dumbRoutes;

    private final java.util.List<CoordinatesInterface> slowCoordinates;

    private final java.util.List<CoordinatesInterface> stoppedCoordinates;

    private final Map<Integer, NodeInterface> nodesById;

    private final DAGShortestPathCalculator shortestPathCalculator;

    // default width and height of the map
    private int width = Config.GRID_WIDTH;
    private int height = Config.GRID_HEIGHT;
    // flags
    private volatile boolean slowActivated;
    private volatile boolean stopActivated;
    private volatile boolean clearNodeActivated;
    private int defaultStartNode;
    private int defaultEndNode;
    private long tickId;


    public Map2D(List<NodeInterface> nodes, List<EdgeInterface> edges, List<RouteInterface> dumbRoutes, List<NodeAttributes> nodeAttributeses) {
        super();

        this.nodes = new ArrayList<NodeInterface>(nodes);
        nodesById = new HashMap<Integer, NodeInterface>();
        for(NodeInterface node: nodes) {
            nodesById.put(node.getId(), node);
        }
        LOG.info("Total nodes in the map: " + nodes.size());
        this.edges = new ArrayList<EdgeInterface>(edges);
        shortestPathCalculator = new DAGShortestPathCalculator(this.nodes, this.edges);
        LOG.info("Total edges in the map: " + edges.size());
        this.dumbRoutes = new ArrayList<RouteInterface>(dumbRoutes);
        slowCoordinates = new ArrayList<CoordinatesInterface>();
        stoppedCoordinates = new ArrayList<CoordinatesInterface>();
        this.nodeAttributes = new ArrayList<NodeAttributes>(nodeAttributeses);

        vehicles = new ArrayList<VehicleInterface>();
        intelligentVehicles = new ArrayList<VehicleInterface>();
        timer = new ScheduledThreadPoolExecutor(1);
        startLineRunner();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                CoordinatesInterface clickCoordinates =
                        new Coordinates(new Pair(e.getX()/Config.NODE_WIDTH, e.getY()/Config.NODE_HEIGHT));
                LOG.info("mouse clicked on MAP: " + clickCoordinates.toString());
                onMouseClicked(clickCoordinates);
            }
        });

    }

    private void onMouseClicked(CoordinatesInterface coordinates) {
        EdgeInterface edge = getEdge(coordinates);
        if( edge == null ) {
            LOG.info("Edge is null for : " + coordinates +
                    ", slowActive " + slowActivated +
                    ", stopActive " + stopActivated);
        } else {
            LOG.info("onMouseClicked: Edge: " + edge.toString());
        }
        if(slowActivated) {
            if(edge != null) {
                LOG.info("*** SLOW ACTIVATED ***");
                slowCoordinates.add(coordinates);
                edge.setSlowCoordinates(coordinates);
            }
            slowActivated = false;
            return;
        }
        if(stopActivated) {
            if(edge != null) {
                LOG.info("*** STOP ACTIVATED: " + edge.toString() +" ***");
                stoppedCoordinates.add(coordinates);
                edge.setStoppedCoordinates(coordinates);
            }
            stopActivated = false;
        }
        if(clearNodeActivated) {
            if(edge != null) {
                LOG.info("*** CLEAR NODE ACTIVATED ***");
                edge.clearEdge();
                slowCoordinates.remove(coordinates);
                stoppedCoordinates.remove(coordinates);
            }
            clearNodeActivated = false;
        }
    }

    private EdgeInterface getEdge(CoordinatesInterface coordinates) {
        for(EdgeInterface edge : edges ) {
            if(edge.contains(coordinates)) {
                return edge;
            }
        }
        return null;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.GRAY);
        Graphics2D g2d = (Graphics2D)g;
        drawGrid(g2d);

        drawNodes(g2d);
        drawEdges(g2d);
        drawVehicles(g2d);
        g2d.setColor(Color.BLACK);
        drawLine(g2d);
        drawSlowCoordinates(g2d);
        drawStopCoordinates(g2d);
        for(NodeAttributes attributes: nodeAttributes) {
            drawNodeIds(g2d, attributes.getName(),
                    attributes.getCoordinates().getX(),
                    attributes.getCoordinates().getY());
        }


    }

    private void drawNodeIds(Graphics2D g2d, String text, int x, int y) {
        g2d.setColor(new Color(255, 255, 255));
        g2d.setFont(new Font("SansSerif", Font.BOLD, 10));
        g2d.drawString(text, x*Config.NODE_WIDTH, (y+1)*Config.NODE_HEIGHT);
    }

    private int pickRoute = 0;
    private AtomicInteger uniqueVehicleId = new AtomicInteger(0);
    private void moveVehicles() {
        try {
            if( (intelligentVehicles.size() < Config.TOTAL_INTELLIGENT_VEHICLES_ALLOWED) &&
                    (vehicles.size() < Config.TOTAL_VEHICLES_ALLOWED &&
                    tickId % 2 == 0) ) {
                try {
                    Vehicle vehicle = new Vehicle(uniqueVehicleId.getAndIncrement(),
                            nodesById.get(defaultStartNode),
                            nodesById.get(defaultEndNode),
                            shortestPathCalculator);
                    vehicles.add(vehicle);
                    intelligentVehicles.add(vehicle);
                } catch(Exception e) {

                }
            }
            if (vehicles.size() < Config.TOTAL_VEHICLES_ALLOWED) {
                RouteInterface hardCodedRoute = dumbRoutes.get(pickRoute++ % dumbRoutes.size());
                vehicles.add(new DumbVehicle(uniqueVehicleId.getAndIncrement(), hardCodedRoute));
            }
            Iterator<VehicleInterface> iterator = vehicles.iterator();
            while (iterator.hasNext()) {
                VehicleInterface vehicle = iterator.next();
                Config.MoveStatus status = vehicle.move(stoppedCoordinates, slowCoordinates);
                switch(status) {
                    case REACHED_DESTINATION:
                        iterator.remove();
                        if(vehicle.getRouteType().equals(VehicleInterface.RouteType.DYNAMIC)) {
                            intelligentVehicles.remove(vehicle);
                        }
                        break;

                    default:
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void drawGrid(Graphics2D g2d) {
        drawVerticleLines(g2d);
        drawHorizontalLines(g2d);
    }

    private void drawVerticleLines(Graphics2D g2d) {
        for(int i=0; i < width; i += Config.NODE_WIDTH) {
            g2d.drawLine(i, 0, i, Config.GRID_HEIGHT);
        }
    }

    private void drawHorizontalLines(Graphics2D g2d) {
        for(int i=0; i < height; i += Config.NODE_HEIGHT) {
            g2d.drawLine(0, i, Config.GRID_WIDTH, i);
        }
    }

    private void drawNodes(Graphics2D g2d) {
        for(NodeInterface node: nodes) {
            node.draw(g2d);
        }
    }

    private void drawEdges(Graphics2D g2d) {
        for(EdgeInterface edge: edges) {
            edge.draw(g2d);
        }
    }

    private void drawVehicles(Graphics2D g2d) {
        for(VehicleInterface vehicle : vehicles) {
            vehicle.draw(g2d);
        }
    }

    private void drawLine(Graphics2D g2d) {
        g2d.drawLine(x1.get(),y1.get(),x2.get(), y2.get());

    }

    private void drawSlowCoordinates(Graphics2D g2d) {
        g2d.setColor(Color.YELLOW);
        for(CoordinatesInterface coordinates: slowCoordinates) {
            int x = coordinates.getX();
            int y = coordinates.getY();
            g2d.fillRect(x * Config.NODE_WIDTH, y * Config.NODE_HEIGHT, Config.NODE_WIDTH, Config.NODE_HEIGHT);
        }
    }

    private void drawStopCoordinates(Graphics2D g2d) {
        g2d.setColor(Color.RED);
        for(CoordinatesInterface coordinates: stoppedCoordinates) {
            int x = coordinates.getX();
            int y = coordinates.getY();
            g2d.fillRect(x * Config.NODE_WIDTH, y * Config.NODE_HEIGHT, Config.NODE_WIDTH, Config.NODE_HEIGHT);
        }
    }

    public void startLineRunner() {
        timer.scheduleAtFixedRate(new Runnable() {
            public void run() {
                onTick();
            }
        }, 0, Config.TICK, TimeUnit.MILLISECONDS);
    }

    private void onTick() {
        tickId++;
        moveVehicles();
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LOG.info("ActionEvent: " + e.getActionCommand());
        String command = e.getActionCommand();
        if(command.equalsIgnoreCase("clear all")) {
            removeAllVehicles();
            removeAllSlowAreas();
            removeAllStopAreas();
        }
        if(command.equalsIgnoreCase("clear node")) {
            onClearNodePressed();
        }
    }

    private void onClearNodePressed() {
        clearNodeActivated = true;
    }

    private void removeAllSlowAreas() {
        slowCoordinates.clear();
    }

    private void removeAllStopAreas() {
        stoppedCoordinates.clear();
    }


    private void removeAllVehicles() {
        for(VehicleInterface vehicle: vehicles) {
            vehicle.removeFromMap();
        }
        vehicles.clear();
        intelligentVehicles.clear();
        for(EdgeInterface edge : edges) {
            edge.clearEdge();
        }
    }

    @Override
    public void onSlowPressed() {
        slowActivated = true;
        LOG.info("slowActivated: " + slowActivated);
    }

    @Override
    public void onStopPressed() {
        stopActivated = true;
        LOG.info("stopActivated: " + stopActivated);
    }

    @Override
    public void onClearPressed() {

    }

    public void setDefaultStartNode(int defaultStartNode) {
        this.defaultStartNode = defaultStartNode;
    }

    public void setDefaultEndNode(int defaultEndNode) {
        this.defaultEndNode = defaultEndNode;
    }
}
