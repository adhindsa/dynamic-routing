package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */
public interface ClearSlowOrStopCommandListener {
    void onClearPressed();
}
