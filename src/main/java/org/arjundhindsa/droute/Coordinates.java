package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;

/**
 * @author adhindsa
 */
public class Coordinates implements CoordinatesInterface {

    private Pair<Integer, Integer> coordinates;

    public Coordinates(Pair<Integer, Integer> xyValues) {
        this.coordinates = xyValues;
    }

    @Override
    public int getX() {
        return coordinates.getX();
    }

    @Override
    public int getY() {
        return coordinates.getY();
    }

    public String toString() {
        return coordinates.toString();
    }

    public boolean equals(Object o) {
        if( o == null ) {
            return false;
        }
        if(!(o instanceof Coordinates)) {
            return false;
        }
        Coordinates c = (Coordinates)o;
        if( this == c) {
            return true;
        }
        if( coordinates.equals(c.coordinates) ) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (coordinates == null ? 0 : coordinates.hashCode());
        return result;
    }
}
