package org.arjundhindsa.droute;

import java.awt.*;

/**
 * @author adhindsa
 */
public class Config {
    public static final int GRID_HEIGHT = 600;
    public static final int GRID_WIDTH = 900;
    public static final int NODE_HEIGHT = 10;
    public static final int NODE_WIDTH = 10;
    public static final long TICK = 250;
    public static final int TOTAL_VEHICLES_ALLOWED = 12;
    public static final int TOTAL_INTELLIGENT_VEHICLES_ALLOWED = 10;
    public static final Color DUMB_VEHICLE_COLOR = new Color(9, 9, 237);
    public static final int SLOW_WIDTH = 50;
    public static final int SLOW_HEIGHT = 50;
    public static final int SLOW_SPEED_WEIGHT = 30;
    public static final int CONTROL_RED_STOP_X = 10;
    public static final int CONTROL_RED_STOP_Y = 200;
    public static final int CONTROL_RED_STOP_WIDTH = 30;
    public static final int CONTROL_RED_STOP_HEIGHT = 30;
    public static final int CONTROL_YELLOW_SLOW_X = 10;
    public static final int CONTROL_YELLOW_SLOW_Y = 250;
    public static final int CONTROL_YELLOW_SLOW_WIDTH = 30;
    public static final int CONTROL_YELLOW_SLOW_HEIGHT = 30;
    public static final Color DYNAMIC_ROUTE_VEHICLE = Color.GREEN;

    public enum MoveStatus {
        MOVED,
        STUCK,
        REACHED_DESTINATION
    }
}
