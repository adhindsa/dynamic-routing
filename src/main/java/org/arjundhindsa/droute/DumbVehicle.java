package org.arjundhindsa.droute;

import java.awt.*;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */

/**
 * It has static hard routes. No route changes on the fly.
 */
public class DumbVehicle extends AbstractVehicle {

    private static final Logger LOG = Logger.getLogger("DumbVehicle");
    private static final Color DEFAULT_COLOR = Config.DUMB_VEHICLE_COLOR;


    public DumbVehicle(int uniqueId, RouteInterface route) {
        super(uniqueId);
        java.util.List<EdgeInterface> edgesOnRoute = route.getEdges();
        this.route = route;
        this.state = State.STOPPED;
        routeType = RouteType.STATIC;
        currentEdge = edgesOnRoute.get(0);
        NodeInterface firstNode = edgesOnRoute.get(0).getStartNode();
        destination = edgesOnRoute.get(edgesOnRoute.size()-1).getEndNode();
        currentLocation = firstNode.getCoordinates();
        LOG.fine("Created dumb vehicle: " + uniqueId);
        currentEdge.add(this);
        movable = true;
    }

    @Override
    public void draw(Graphics2D g2d) {
        LOG.fine("drawing DUMB vehicle: " + getVehicleID() + " at location: " + currentLocation);
        g2d.setColor(DEFAULT_COLOR);
        g2d.fillRect(currentLocation.getX()* Config.NODE_WIDTH,
                currentLocation.getY()*Config.NODE_HEIGHT, Config.NODE_WIDTH, Config.NODE_HEIGHT);
    }

    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        if(!(o instanceof DumbVehicle)) {
            return false;
        }
        if(o == this) {
            return true;
        }
        DumbVehicle d = (DumbVehicle)o;
        if(uniqueVehicleId == d.uniqueVehicleId) {
            return true;
        }
        return false;
    }

    @Override
    protected RouteInterface getNewRoute(NodeInterface startNode) {
        return null;
    }
}
