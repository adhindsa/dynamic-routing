package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */
public interface VehicleInterface extends DrawableInterface {

    public enum State {
        MOVING,
        STOPPED
    }

    public enum RouteType {
        STATIC,
        DYNAMIC
    }

    /**
     *
     * @return Current location co-ordinates
     */
    CoordinatesInterface getCurrentLocation();

    /**
     *
     * @return Current state of the vehicle i.e. stopped or
     * moving
     */
    State getState();

    /**
     *
     * @return Gets the route type i.e. Static or dynamic
     */
    RouteType getRouteType();

    /**
     *
     * @return Unique vehicle ID. This helps in distinguishing
     * multiple vehicles on the map from each other.
     */
    int getVehicleID();

    /**
     * Sets the new location of the vehicle
     * @param coordinates
     */
    void setLocation(CoordinatesInterface coordinates);

    /**
     * Sets the destination of the vehicle.
     * @param destination
     */
    void setDestination(NodeInterface destination);

    /**
     * Move the vehicle forward based on it's speed and
     * available slot. If the slot where vehicle is intending
     * to move to is already occupied, then vehicle stays
     * at it's current location.
     */
    Config.MoveStatus move(java.util.List<CoordinatesInterface> stoppedAreas, java.util.List<CoordinatesInterface> slowAreas);

    void removeFromMap();
}
