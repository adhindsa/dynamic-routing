package org.arjundhindsa.droute;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public class ControlPanel extends JPanel {

    private static final Logger LOG = Logger.getLogger("ControlPanel");

    private MouseListener listener;
    private java.util.List<ActionListener> actionListeners;
    private java.util.List<JButton> buttons;
    private java.util.List<JLabel> labels;
    private SlowCommandListener slowCommandListener;
    private StopCommandListener stopCommandListener;

    public ControlPanel() {
        actionListeners = new ArrayList<ActionListener>();
        buttons = new ArrayList<JButton>();
        labels = new ArrayList<JLabel>();
        Box box = Box.createVerticalBox();
        addButton(box, "Submit");
        addButton(box, "Clear All");
        addButton(box, "Clear Node");


        add(box, BorderLayout.CENTER);
//        addIcon(box, "./slowTrafficIcon.jpg", "slow");
//        addIcon(box, "./stopTrafficIcon.jpg", "stop");
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                onMouseClicked(e.getX(), e.getY());
            }
        });
    }

    private void onMouseClicked(int x, int y) {
        // red click
        int baseX = Config.CONTROL_RED_STOP_X;
        int baseY = Config.CONTROL_RED_STOP_Y;
        if(x >= baseX &&
                y >= baseY &&
                x <= baseX + Config.CONTROL_RED_STOP_WIDTH &&
                y <= baseY + Config.CONTROL_RED_STOP_HEIGHT ) {
            stopCommandListener.onStopPressed();
            return;
        }
        // yellow click
        baseX = Config.CONTROL_YELLOW_SLOW_X;
        baseY = Config.CONTROL_YELLOW_SLOW_Y;
        if(x >= baseX &&
                y >= baseY &&
                x <= baseX + Config.CONTROL_YELLOW_SLOW_WIDTH &&
                y <= baseY + Config.CONTROL_YELLOW_SLOW_HEIGHT ) {
            slowCommandListener.onSlowPressed();
            return;
        }
    }

    public void setActionListener(ActionListener actionListener) {
        actionListeners.add(actionListener);
        for(JButton button : buttons) {
            button.addActionListener(actionListener);
        }
    }

    public void setSlowCommandListener(SlowCommandListener slowCommandListener) {
        this.slowCommandListener = slowCommandListener;
    }

    public void setStopCommandListener(StopCommandListener stopCommandListener) {
        this.stopCommandListener = stopCommandListener;
    }

    private void addIcon(Box box, String filePath, final String text) {
        Image myPicture = null;
        try {
            myPicture = ImageIO.read(new File(filePath));
            Image small = myPicture.getScaledInstance(Config.SLOW_WIDTH, Config.SLOW_HEIGHT,1);
            ImageIcon icon = new ImageIcon(small);

            JLabel picLabel = new JLabel(icon, JLabel.CENTER);
            picLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    LOG.info("mouseClicked --- " + text);
                    if(text.equalsIgnoreCase("slow")) {
                        slowCommandListener.onSlowPressed();
                    }
                    if(text.equalsIgnoreCase("stop")) {
                        stopCommandListener.onStopPressed();
                    }
                }

//                @Override
//                public void mousePressed(MouseEvent e) {
//                    super.mousePressed(e);
//                    LOG.info("MousePressed --- " + text);
//                    if(text.equalsIgnoreCase("slow")) {
//                        slowCommandListener.onSlowPressed();
//                    }
//                    if(text.equalsIgnoreCase("stop")) {
//                        stopCommandListener.onStopPressed();
//                    }
//                }
            });
            box.add(picLabel);
            labels.add(picLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void addButton(Box box, final String text) {
        JButton button = new JButton();
        button.setLayout(new BorderLayout());
        button.setText(text);
        for(ActionListener listener: actionListeners) {
            button.addActionListener(listener);
        }
        box.add(button);
        buttons.add(button);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(new Color(140, 190, 239));
        Graphics2D g2d = (Graphics2D) g;
        addStopRectangle(g2d);
        int x = Config.CONTROL_RED_STOP_X+35;
        int y = Config.CONTROL_RED_STOP_Y+15;
        drawControlString(g2d, "Stop Node", x, y);
        x = Config.CONTROL_YELLOW_SLOW_X+35;
        y = Config.CONTROL_YELLOW_SLOW_Y+15;
        drawControlString(g2d, "Slow Node", x, y);
        x = Config.CONTROL_YELLOW_SLOW_X;
        y = Config.CONTROL_YELLOW_SLOW_Y+50;
        drawControlString(g2d, "Note: After clicking on Stop",x, y);
        y = y + 10;
        drawControlString(g2d, "or Slow Node, click on the",x, y);
        y = y + 10;
        drawControlString(g2d, "black path/edge on the",x, y);
        y = y + 10;
        drawControlString(g2d, "left pane ",x, y);
        y = y + 20;
        drawDumbVehicle(g2d, x, y);
        drawControlString(g2d, "DumbVehicle with ",x+15, y+5);
        y = y + 10;
        drawControlString(g2d, "static route ",x+15, y+5);
        y = y + 20;
        drawVehicle(g2d, x, y);
        drawControlString(g2d, "Intelligent vehicle with", x+15, y+5);
        y = y + 10;
        drawControlString(g2d, "dynamic route ",x+15, y+5);
        addSlowRectangle(g2d);
    }

    private void drawDumbVehicle(Graphics2D g2d, int x, int y) {
        g2d.setColor(Config.DUMB_VEHICLE_COLOR);
        g2d.fillRect(x, y, Config.NODE_WIDTH, Config.NODE_HEIGHT);
    }

    private void drawVehicle(Graphics2D g2d, int x, int y) {
        g2d.setColor(Color.GREEN);
        g2d.fillRect(x, y, Config.NODE_WIDTH, Config.NODE_HEIGHT);
    }

    private void drawControlString(Graphics2D g2d, String text, int x, int y) {
        g2d.setColor(new Color(255, 255, 255));
        g2d.setFont(new Font("SansSerif", Font.BOLD, 10));
        g2d.drawString(text, x, (y+1));
    }

    private void addStopRectangle(Graphics2D g2d) {
        g2d.setColor(Color.RED);
        int x = Config.CONTROL_RED_STOP_X;
        int y = Config.CONTROL_RED_STOP_Y;
        g2d.fillRect(x, y, Config.CONTROL_RED_STOP_WIDTH,  Config.CONTROL_RED_STOP_HEIGHT);

    }

    private void addSlowRectangle(Graphics2D g2d) {
        g2d.setColor(Color.YELLOW);
        int x = Config.CONTROL_YELLOW_SLOW_X;
        int y = Config.CONTROL_YELLOW_SLOW_Y;
        g2d.fillRect(x, y, Config.CONTROL_RED_STOP_HEIGHT, Config.CONTROL_YELLOW_SLOW_WIDTH);
    }

}
