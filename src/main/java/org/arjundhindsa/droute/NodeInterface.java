package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */

/**
 * Represents a node on a graph.
 */
public interface NodeInterface extends DrawableInterface {
    /**
     *
     * @return The unique ID of the node
     */
    int getId();

    /**
     *
     * @return Coordinates of a node on the Map.
     */
    CoordinatesInterface getCoordinates();
}
