package org.arjundhindsa.droute;

import java.awt.*;
import java.util.logging.Logger;

/**
 * @author adhindsa
 */
public class Vehicle extends AbstractVehicle {

    private static final Logger LOG = Logger.getLogger("Vehicle");
    private final DAGShortestPathCalculator routeCalculator;

    public Vehicle(int uniqueId, NodeInterface startNode, NodeInterface endNode,
                   DAGShortestPathCalculator routeCalculator) throws Exception {
        super(uniqueId);
        routeType = RouteType.DYNAMIC;
        currentLocation = startNode.getCoordinates();
        destination = endNode;
        this.routeCalculator = routeCalculator;
        route = routeCalculator.getShortestRoute(startNode, endNode);
        java.util.List<EdgeInterface> edgesOnRoute = route.getEdges();
        if( edgesOnRoute.size() == 0 ) {
            LOG.warning("Unable to create new vehicle route...abandoning the new vehicle");
            throw new Exception("Unable to create new vehicle");
        }
        currentEdge = edgesOnRoute.get(0);
        LOG.fine("Created intelligent vehicle: " + uniqueId);
        currentEdge.add(this);
        movable = true;
    }

    @Override
    public void draw(Graphics2D g2d) {
        g2d.setColor(Config.DYNAMIC_ROUTE_VEHICLE);
        g2d.fillRect(currentLocation.getX()* Config.NODE_WIDTH,
                currentLocation.getY()*Config.NODE_HEIGHT, Config.NODE_WIDTH, Config.NODE_HEIGHT);
    }


    @Override
    protected RouteInterface getNewRoute(NodeInterface startNode) {
        return routeCalculator.getShortestRoute(startNode, destination);
    }
}
