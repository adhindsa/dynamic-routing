package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */
public interface SlowCommandListener {
    void onSlowPressed();
}
