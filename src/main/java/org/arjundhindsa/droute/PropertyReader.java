package org.arjundhindsa.droute;

/**
 * @author adhindsa
 */

import org.arjundhindsa.droute.*;
import org.arjundhindsa.utils.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Reads config properties from a text input file e.g. input.txt
 */
public class PropertyReader {

    private static final Logger LOG = Logger.getLogger("PropertyReader");
    private static final String DEFAULT_DELIMITER = ",";

    private final String configFile;
    private Properties properties;
    private int totalNodes;
    private int totalEdges;
    private int totalDumbRoutes;

    private List<NodeInterface> nodes;
    private List<EdgeInterface> edges;
    private List<RouteInterface> dumbRoutes;
    private int defaultStartNode;
    private int defaultEndNode;

    private List<NodeAttributes> allNodesAttributes;

    public PropertyReader(String propertyFile) {
        this.configFile = propertyFile;
        properties = new Properties();
        nodes = new ArrayList<NodeInterface>();
        edges = new ArrayList<EdgeInterface>();
        dumbRoutes = new ArrayList<RouteInterface>();
        allNodesAttributes = new ArrayList<NodeAttributes>();
    }

    public void load() throws FileNotFoundException, IOException {
        properties.load(new FileInputStream(configFile));
        loadNodes();
        loadEdges();
        loadDumbRoutes();
        loadDefaultStartEndNode();
    }

    private void loadDefaultStartEndNode() {
        defaultStartNode = Integer.parseInt(properties.getProperty("default.startNode"));
        defaultEndNode = Integer.parseInt(properties.getProperty("default.endNode"));
    }

    private void loadEdges() {
        totalEdges = Integer.parseInt(properties.getProperty("totalEdges"));
        for(int i=0; i < totalEdges; i++) {
            String[] pair = getStringArray("edge." + i, DEFAULT_DELIMITER);
            if(pair == null) {
//                throw new IllegalStateException("Unable to parse a EDGE: ID " + i);
                continue;
            }
            if(pair.length != 2) {
                throw new IllegalStateException("Edge value provided incorrect format: ID " + i +
                        ", value " + Arrays.toString(pair));
            }
            NodeInterface n1 = findNode(Integer.parseInt(pair[0]));
            NodeInterface n2 = findNode(Integer.parseInt(pair[1]));
            if(n1 != null && n2 != null) {
                Edge e = new Edge(i, n1, n2);
                EdgeInterface foundExisting = findEdgesWithSameNodes(n1,n2);
                if(foundExisting != null) {
                    throw new IllegalStateException("Found already existing edge: " + foundExisting.toString());
                }
                edges.add(e);
            }
        }

    }

    private EdgeInterface findEdgesWithSameNodes(NodeInterface start, NodeInterface end) {
        for(EdgeInterface edge: edges) {
            if( edge.getStartNode().equals(start) && edge.getEndNode().equals(end)) {
                return edge;
            }
        }
        return null;
    }

    private NodeInterface findNode(int nodeId) {
        for(NodeInterface node: nodes) {
            if(node.getId() == nodeId) {
                return node;
            }
        }
        return null;
    }

    private EdgeInterface findEdge(int edgeId) {
        for(EdgeInterface edge: edges) {
            if(edge.getEdgeId() == edgeId) {
                return edge;
            }
        }
        return null;
    }

    private void loadNodes() {
        totalNodes = Integer.parseInt(properties.getProperty("totalNodes"));
        LOG.info("Total nodes loading from the graph: " + totalNodes);

        for(int i=0; i < totalNodes; i++) {
            String[] pair = getStringArray("node." + i + ".coordinates", DEFAULT_DELIMITER);
//            String name = properties.getProperty("node." + i + ".name");

            if(pair == null || pair.length != 3 || pair[2] == null ) {
                throw new IllegalStateException("Unable to parse a NODE: ID " + i);
            }
            String name = pair[2];
//            String[] pair2 = getStringArray("node." + i + ".coordinates", DEFAULT_DELIMITER);
            String[] pair2 = new String[] { pair[0], pair[1] };
            Pair<Integer, Integer> intPair = new Pair(Integer.parseInt(pair[0]), Integer.parseInt(pair[1]));
            Pair<Integer, Integer> attribPair = new Pair(Integer.parseInt(pair2[0]), Integer.parseInt(pair2[1]));
            Node n = new Node(i, new Coordinates(intPair));
            NodeAttributes attributes = new NodeAttributes(name, attribPair);
            allNodesAttributes.add(attributes);
            nodes.add(n);
        }
    }

    public List<NodeInterface> getNodes() {
        return nodes;
    }

    public List<EdgeInterface> getEdges() {
        return edges;
    }

    public List<RouteInterface> getDumbRoutes() { return dumbRoutes; };

    public int getDefaultStartNode() {
        return defaultStartNode;
    }

    public int getDefaultEndNode() {
        return defaultEndNode;
    }

    public List<NodeAttributes> getAllNodesAttributes() {
        return allNodesAttributes;
    }

    private String[] getStringArray(String key, String delimiter) {
        String strValue = properties.getProperty(key);
        if(strValue == null) {
            return null;
        }
        return strValue.split(delimiter);
    }

    public void loadDumbRoutes() {
        totalDumbRoutes = Integer.parseInt(properties.getProperty("dumbRoutes"));
        LOG.info("Total dumb routes loading from the graph: " + totalDumbRoutes);

        for(int i=0; i < totalDumbRoutes; i++) {
            String[] routeNodes = getStringArray("dumbRoute." + i, DEFAULT_DELIMITER);
            if(routeNodes == null) {
                throw new IllegalStateException("Unable to parse a DUMB ROUTES: ID " + i);
            }
            List<EdgeInterface> routePath = new ArrayList<EdgeInterface>();
            for(String nodeId : routeNodes) {
                EdgeInterface edge = findEdge(Integer.parseInt(nodeId));
                LOG.info("Loading edge: " + edge.toString());
                if(edge == null) {
                    throw new IllegalStateException("Could not find node: " + nodeId);
                }
                routePath.add(edge);
            }
            Route route = new Route(routePath);
            dumbRoutes.add(route);
        }
    }
}
