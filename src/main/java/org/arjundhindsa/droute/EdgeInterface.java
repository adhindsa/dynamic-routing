package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;

/**
 * @author adhindsa
 */

/**
 * Edges are directional so each edge has starting
 * and an end.
 */
public interface EdgeInterface extends DrawableInterface {
    Pair<NodeInterface, NodeInterface> getEdgeNodes();
    NodeInterface getStartNode();
    NodeInterface getEndNode();
    int getEdgeId();
    CoordinatesInterface getNextCoordinates(CoordinatesInterface currentCoordinates);
    void add(VehicleInterface vehicle);
    boolean isOccupied(CoordinatesInterface coordinates);
    void remove(VehicleInterface vehicle);
    boolean contains(CoordinatesInterface coordinates);

    int getEdgeWeight();

    void setSlowCoordinates(CoordinatesInterface coordinates);
    void setStoppedCoordinates(CoordinatesInterface coordinates);
    void clearEdge();
    void clearEdge(CoordinatesInterface coordinates);
    boolean stopped();
}
