package org.arjundhindsa.droute;

import org.arjundhindsa.utils.Pair;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author adhindsa
 */
public class DAGShortestPathCalculatorTest {

    private TestNode n1, n2, n3, n4, n5;
    private TestEdge e1, e2, e3, e4, e5;
    private java.util.List<NodeInterface> nodes;
    private java.util.List<EdgeInterface> edges;
    private DAGShortestPathCalculator dag;

    @BeforeClass
    public static void setUp() {

    }

    @Before
    public void initialize() {
        nodes = new ArrayList<NodeInterface>();
        n1 = new TestNode(0);
        n2 = new TestNode(1);
        n3 = new TestNode(2);
        n4 = new TestNode(3);
        n5 = new TestNode(4);
        nodes.add(n1);
        nodes.add(n2);
        nodes.add(n3);
        nodes.add(n4);
        nodes.add(n5);

        edges = new ArrayList<EdgeInterface>();
        e1 = new TestEdge(n1, n3, 4);
        e2 = new TestEdge(n1, n2, 3);
        e3 = new TestEdge(n3, n4, 3);
        e4 = new TestEdge(n2, n4, 5);
        e5 = new TestEdge(n4, n5, 2);
        edges.add(e1);
        edges.add(e2);
        edges.add(e3);
        edges.add(e4);
        edges.add(e5);

        dag = new DAGShortestPathCalculator(nodes, edges);
    }

    @Test
    public void TestShortestPath() {
        RouteInterface shortestRoute = dag.getShortestRoute(n1, n5);
        assertTrue(shortestRoute != null);
        assertEquals(3, shortestRoute.getRouteLength());

        shortestRoute = dag.getShortestRoute(n2, n5);
        assertEquals(2, shortestRoute.getRouteLength());
    }

    /////////////////////////////////////////////////////////
    private class TestNode implements NodeInterface {

        private final int nodeId;

        public TestNode(int nodeId) {
            this.nodeId = nodeId;
        }

        @Override
        public int getId() {
            return nodeId;
        }

        @Override
        public CoordinatesInterface getCoordinates() {
            return null;
        }

        @Override
        public void draw(Graphics2D g2d) {

        }

        public String toString() {
            return "[" + nodeId + "]";
        }
    }


    /////////////////////////////////////////////////////////
    private class TestEdge implements EdgeInterface {

        TestNode from;
        TestNode to;
        int weight;
        public TestEdge(TestNode from, TestNode to, int weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
        }

        @Override
        public Pair<NodeInterface, NodeInterface> getEdgeNodes() {
            return new Pair(from, to);
        }

        @Override
        public NodeInterface getStartNode() {
            return from;
        }

        @Override
        public NodeInterface getEndNode() {
            return to;
        }

        @Override
        public int getEdgeId() {
            return -1;
        }

        @Override
        public CoordinatesInterface getNextCoordinates(CoordinatesInterface currentCoordinates) {
            return null;
        }

        @Override
        public void add(VehicleInterface vehicle) {

        }

        @Override
        public boolean isOccupied(CoordinatesInterface coordinates) {
            return false;
        }

        @Override
        public void remove(VehicleInterface vehicle) {

        }

        @Override
        public boolean contains(CoordinatesInterface coordinates) {
            return false;
        }

        @Override
        public int getEdgeWeight() {
            return weight;
        }

        @Override
        public void setSlowCoordinates(CoordinatesInterface coordinates) {

        }

        @Override
        public void setStoppedCoordinates(CoordinatesInterface coordinates) {

        }

        @Override
        public void clearEdge() {

        }

        @Override
        public void clearEdge(CoordinatesInterface coordinates) {

        }

        @Override
        public boolean stopped() {
            return false;
        }

        @Override
        public void draw(Graphics2D g2d) {

        }

        public String toString() {
            return "[" + from + "," + to + "]";
        }
    }
}
