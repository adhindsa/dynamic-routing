# README #

Autonomously Driven Vehicles (ADVs) will be making their way into our everyday lives in the near future. Companies like Google are already experimenting with driverless cars on the streets. Still in its early stages, ADVs pose many challenges to engineers. Through this programming project I intend to address one such challenge. With computers behind the wheel, ADVs need to dynamically re-route themselves in case of an accident or a blocked route to avoid triggering deadlock situations or a traffic jam.
In this project I have designed a prototype that uses a Directed Acyclic Graph (DAG) data structure. The roads are represented by directed edges of the graph and intersections are represented as nodes of a Graph. To find the shortest route between two points on the map, I have used Dijkstra’s shortest path algorithm. When an accident occurs the edge nodes are notified of the abnormal situation. If the edge is completely blocked the ADVs that are capable of dynamic re-routing get notified before entering the blocked road. The new shortest path is calculated and instead of entering the road with an accident or a traffic jam, the ADVs will take a detour by following next shortest path available.

### What is this repository for? ###

This repository contains a Simulation for an infrastructure how ADVs can get notified and re-route themselves dynamically.

### How do I get set up? ###

The source code in this repository is standalone with no external dependencies. To download the source, use the following git command to clone it onto your computer:
*
git clone https://adhindsa@bitbucket.org/adhindsa/dynamic-routing.git*

Once you have the source tree download. 

*bash-3.2$ cd dynamic-routing/*

You can either use maven to build it with following set of commands:

*bash-3.2$ mvn clean install*

If you do not have maven on your computer, you can still compile using the following command:
*
bash-3.2$ cd src/main/java
bash-3.2$ javac -d ../../../target/ org/arjundhindsa/droute/SimulationDriver.java*

Use the following command to actually run the Simulation:

*bash-3.2$ java -cp ../../../target org.arjundhindsa.droute.SimulationDriver --config ../../../input2.txt*

Use -d option to javac to put the compiled class in a directory of your choice.

When running the application using java, use the destination directory where you put the class files, as your classpath (-cp option)

To provide the config file to the Simulation, use --config option and provide the path to your config file, which in our case was sitting on the base of the source directory.

When the Simulation starts up successfully, the sample application screenshot will look like:

![dynamic_routing_1.png](https://bitbucket.org/repo/K4Gnrp/images/1455953842-dynamic_routing_1.png)

### Who do I talk to? ###

* Repo owner (Arjun Dhindsa)